libclass-inner-perl (0.200001-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libclass-inner-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 17:38:39 +0100

libclass-inner-perl (0.200001-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 11 Jun 2022 22:30:47 +0100

libclass-inner-perl (0.200001-2) unstable; urgency=medium

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to dpkg source format 3.0 (quilt)
  * Update to Standards-Version 4.1.3

 -- Niko Tyni <ntyni@debian.org>  Thu, 18 Jan 2018 16:13:12 +0200

libclass-inner-perl (0.200001-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Standards-Version 3.8.3 (drop perl version dep)
  * Add myself to Uploaders and Copyright
  * Bump to debhelper 7 and use short rules format
  * Remove patch; the issue has been taken care of upstream

  [ gregor herrmann ]
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Change my email address.

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 21 Nov 2009 19:30:44 -0500

libclass-inner-perl (0.1-5) unstable; urgency=low

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/rules: delete /usr/lib/perl5 only if it exists.
  * Create patch test_basic_isa.patch to make the test work with perl 5.10
    which has changed inheritance detection ("->isa()") (closes: #467150).
    Add quilt framework for the patch.
  * Set Standards-Version to 3.7.3 (no changes).
  * Set debhelper compatibility level to 6.
  * debian/watch: use dist-based URL.
  * debian/rules:
    - remove compiler flags, this package is arch:all
    - remove spurious whitespace, commented out dh_* calls
    - move tests to build-stamp target
    - use $@ for touching stamp-files
    - don't ignore errors of make distclean
    - move dh_clean before make distclean
    - don't install README any more, no useful information for users
  * debian/copyright: change to new format, add copyright information for
    the packaging.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 23 Feb 2008 18:25:00 +0100

libclass-inner-perl (0.1-4) unstable; urgency=low

  * Moved debhelper to Build-Depends.
  * Set Standards-Version to 3.7.2 (no changes).

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 16 Jun 2006 14:46:54 +0200

libclass-inner-perl (0.1-3) unstable; urgency=low

  * New maintainer (closes: #357083).
  * Bumped Standards-Version and Debhelper compatibility level.
  * Fixed copyright statement in debian/copyright.
  * Updated debian/rules.
  * Added watch file.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 17 Mar 2006 20:06:41 +0100

libclass-inner-perl (0.1-2) unstable; urgency=high

  * Fix Build-Depends by deleting my hacked dpkg-source.

 -- Chip Salzenberg <chip@debian.org>  Sun,  5 Oct 2003 21:43:55 -0400

libclass-inner-perl (0.1-1) unstable; urgency=low

  * Initial Release.

 -- Chip Salzenberg <chip@debian.org>  Fri,  3 Oct 2003 18:58:19 -0400
